def is_leap(year: int) -> bool:
    """Provides insight to whether
        the int year is on a leap
        year or not.
    
    Arguments:
        year {int} -- [year in as yyyy]
    
    Returns:
        bool -- [Indicator of whether or not the year is a leap year]
    """
    leap = False

    if year % 400 == 0:
        leap = True
    elif year % 100 == 0:
        leap = False
    elif year % 4 == 0:
        leap = True
    else:
        leap = False

    return leap

if __name__ == '__main__':
    year = int(input())

    print(is_leap(year))