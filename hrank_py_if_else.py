def parity(myNum: int) -> str:
    """Provide insight on whether myNum is even or odd
    
    Arguments:
        myNum {int} -- [odd or even int]
    
    Returns:
        str -- [returning a str to indicate whether myNum is odd or even]
    """
    parityVal = "even" if myNum % 2 == 0 else "odd"
    return parityVal

def weird_indicator(myNum: int) -> str:
    """Provides insight on whether myNum is weird or not weird
        based off preset rules
    
    Arguments:
        myNum {int} -- [int to be put through weird not weird logic]
    
    Returns:
        str -- [returns a value of either Weird or Not Weird]
    """
    WEIRD = "Weird"
    NOT_WEIRD = "Not Weird"
    if parity(myNum) == "odd":
        return WEIRD
    elif myNum in range(1,6):
        return NOT_WEIRD
    elif myNum in range(6,21):
        return WEIRD
    else:
        return NOT_WEIRD

n = int(input())
print(weird_indicator(n))