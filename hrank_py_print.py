def return_int(num: int) -> list:
    """Returns a list of ints

    Arguments:
        num {int} -- [range to end loop on]

    Returns:
        list -- [List of ints]
    """
    finNum = []
    for currNum in range(1, num + 1):
        finNum.append(currNum)
    return finNum

if __name__ == '__main__':
    n = int(input())
    # Provides an int from 1 to n
    print(*return_int(n), sep='', end='\n')
    print(*return_int(n))
    print(return_int(n))

    # This code is another possible solve, but with str method
    # myVar = int(''.join(str(x) for x in return_int(n)))
    # print(myVar)