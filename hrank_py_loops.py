def int_sqr(num1: int) -> int:
    """Provides sqr of num1
    
    Arguments:
        num1 {int} -- [int to be sqrd]
    
    Returns:
        int -- [sqrd int]
    """
    finNum = num1 * num1
    return finNum

if __name__ == '__main__':
    a = int(input())

    # Looping through integers and executes int_sqr func up until the provided input of a
    for currInt in range(0, a):
        print(int_sqr(currInt))