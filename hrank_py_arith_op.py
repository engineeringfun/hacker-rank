def sum_nums(num1: int, num2: int) -> int:
    """Provide the sum for two integers
    
    Arguments:
        num1 {int} -- [initial int]
        num2 {int} -- [secondary int]
    
    Returns:
        int -- [sum of two integers]
    """
    finNum = num1 + num2
    return finNum

def diff_nums(num1: int, num2: int) -> int:
    """Provide the diff for two integers
    
    Arguments:
        num1 {int} -- [int to subtract from]
        num2 {int} -- [int to be subtracted]
    
    Returns:
        int -- [the diff of two integers]
    """
    finNum = num1 - num2
    return finNum

def product_nums(num1: int, num2: int) -> int:
    """Provide the product for two integers
    
    Arguments:
        num1 {int} -- [initial int]
        num2 {int} -- [secondary int]
    
    Returns:
        int -- [the product of two integers]
    """
    finNum = num1 * num2
    return finNum

if __name__ == '__main__':
    a = int(input())
    b = int(input())

    print(sum_nums(a, b))
    print(diff_nums(a, b))
    print(product_nums(a, b))