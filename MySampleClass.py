class MySampleClass:
    """A simple example of a class"""
    i = 12345
    c = 10105

    def f(self):
        return {
            'first_var': self.i
            , 'second_var': self.c
            , 'priceTags': "Card Type_1_Supporter, Pokemon Singles, Price_2_Between $0.25 and $0.49, Price_3_Between $0.50 and $0.99, Product Line_1_Pokemon, Rarity_2_Uncommon, Set Name_1_SM - Burning Shadows, Price_3_Between $0.50 and $0.99"
        }