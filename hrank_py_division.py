def int_div(num1: int, num2: int) -> int:
    """Provides the quotient of integer division
    
    Arguments:
        num1 {int} -- [dividend]
        num2 {int} -- [divisor]
    
    Returns:
        int -- [quotient]
    """
    finNum = num1 // num2
    return finNum

def float_div(num1: int, num2: int) -> float:
    """Provides the quotient of float division
    
    Arguments:
        num1 {int} -- [dividend]
        num2 {int} -- [divisor]
    
    Returns:
        int -- [quotient]
    """
    finNum = num1 / num2
    return finNum

if __name__ == '__main__':
    a = int(input())
    b = int(input())

    print(int_div(a, b))
    print(float_div(a, b))